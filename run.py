import argparse
import importlib
import inspect
import pathlib

from ciphers import Cipher


def main():
    # Start with an empty dictionary, we're gonna populate later
    cipher_dict = {}
    # Basically read as "for every path in the directory I've supplied"
    for p in pathlib.Path('ciphers').iterdir():
        # The above returns a Path object which has some attributes like name, we're looking
        # for any .py files that aren't the package file.
        if p.name.endswith('.py') and p.name != '__init__.py':
            # Python dynamic import, this is like runtime "using" statements and you can
            # use all of the code you import after doing this.
            mod = importlib.import_module(f'ciphers.{p.name[:-3]}')
            # Some python introspection stuff, we're finding any member in the object we pass
            # it that satisfies the predicate "isclass", basically finding all classes in the
            # module.
            classes = inspect.getmembers(mod, inspect.isclass)
            # The above returns a list of tuples, [(a, b), (c, d)] and so on. Python supports
            # iterative unpacking of types like lists and tuples, so here we iterate over each
            # tuple in the list, and unpack each with the first element in name, second in cls.
            for name, cls in classes:
                # More type inspection. If we find a subclass of Cipher, map to its constructor
                # by name in our dictionary.
                if issubclass(cls, Cipher) and cls != Cipher:
                    # Said mapping
                    cipher_dict[name] = cls

    argparser = argparse.ArgumentParser("Cipher test entry point")
    argparser.add_argument("method", choices=["encrypt", "decrypt"], help="The cipher mechanism to use.")
    argparser.add_argument("input", help="The input to encrypt or decrypt.")
    subparsers = argparser.add_subparsers(dest="cipher")

    for name, cls in cipher_dict.items():
        cipher_parser = subparsers.add_parser(name)
        cls.update_args(cipher_parser)

    options = argparser.parse_args()

    # Look up the constructor in our dictionary we populated. This will give us 'cipher'
    # as a callable, specifically the constructor for the class we found earlier. Note that
    # classes in python may only have one constructor, and method overloading does not exist.
    cipher = cipher_dict.get(options.cipher)
    # Safety checks, dict.get is a convenience method that avoids throwing a KeyError if the
    # key doesn't exist and instead returns the python unique value None.
    if cipher is None:
        # If we didn't find the key bail out
        raise ValueError(f"Requested cipher '{options.cipher}' not found")


    # TODO: Supply the args to the constructor here
    c = cipher()

    # This is nutty, but we're basically looking up the function name by string and calling it.
    # Basically it resolves to either 'c.encrypt()' or 'c.decrypt()'. Python is cool.
    # Read as "get the attribe on the object c with string name options.method (either "encrypt"
    # or "decrypt" here).
    method = getattr(c, options.method)
    # Actually do the operation
    output = method(options.input)

    print(f"Using cipher '{options.cipher}' to perform method '{options.method}'")
    print(f"Input : '{options.input}'")
    print(f"Result: '{output}'")


# Python code is actually executed on import. This guard is common in code that is meant to be
# both run as a script or entry point and also just a good practice.
if __name__ == "__main__":
    main()
