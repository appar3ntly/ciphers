"""Package for implementation of various ciphers.

Ciphers can be found at:

    http://practicalcryptography.com/ciphers/classical-era/

Concrete cipher implementations should be implemented in their own source files in this package
and should contain a class subclassing the included Cipher class in this package level file.
"""
import abc


class Cipher(abc.ABC):
    """Base class for implementing cipher variants.

    Defines the interface elements that all Cipher subclasses must implement.
    """
    @classmethod
    def update_args(cls, argparser):
        return

    def encrypt(self, plaintext):
        """Abstract interface base for encrypt function.

        Required to be implemented by child classes.

        Args:
            plaintext: The unencrypted text to encrypt using the cipher.
        Returns:
            The encrypted ciphertext.
        """
        raise NotImplementedError

    def decrypt(self, ciphertext, **kwargs):
        """Abstract interface base for decrypt function.

        Required to be implemented by child classes.

        Args:
            plaintext: The unencrypted text to encrypt using the cipher.
        Returns:
            The encrypted ciphertext.
        """
        raise NotImplementedError
