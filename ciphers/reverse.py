from . import Cipher


class Reverse(Cipher):
    """Example cipher implementing a simple string reversal."""
    def encrypt(self, plaintext):
        """Reverse the supplied plaintext string.

        Args:
            plaintext: The string to reverse.
        Returns:
            The plaintext string with reversed character order.
        """
        return "".join([plaintext[i] for i in range(len(plaintext) - 1, -1, -1)])

    def decrypt(self, ciphertext):
        return self.encrypt(ciphertext)
